module.exports = {
  /*
   ** Headers of the page
   */
  head: {
    title: 'Zedboard',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Zedboard Website'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },
  /*
   ** Global CSS
   */
  css: [{
    src: '~/assets/css/main.scss',
    lang: 'scss'
  }],
  /*
   ** Customize the progress bar color
   */
  loading: {
    color: '#3B8070'
  },
  /*
   ** Modules configuration
   */
  modules: [
    ['storyblok-nuxt', {
      accessToken: 'XMiorK381cMVM3ZV4NgBpQtt',
      cacheProvider: 'memory'
    }],
    '@nuxtjs/font-awesome'
  ],
  /*
   ** Middleware configuration
   */
  router: {
    middleware: 'getNavigation'
  },
  /*
   ** Plugins configuration
   */
  plugins: [
    '~/plugins/components',
    {
      src: '~/plugins/siema',
      ssr: false
    }
  ],
  /*
   ** Build configuration
   */
  build: {
    postcss: {
      plugins: {
        'postcss-custom-properties': {
          warnings: false
        }
      }
    },
    /*
     ** Run ESLint on save
     */
    extend(config, {
      isDev,
      isClient
    }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
