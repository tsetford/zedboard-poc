import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      main_nav: []
    },
    mutations: {
      setNavigation(state, main_nav) {
        state.main_nav = main_nav
      }
    },
    actions: {
      loadNavigation({
        commit
      }, context) {
        return this.$storyapi.get(`cdn/stories/navigation/main`, {
          version: context.version
        }).then((res) => {
          commit('setNavigation', res.data.story.content)
        })
      }
    }
  })
}

export default createStore
