import Vue from 'vue'
import Page from '~/components/Page.vue'
import Row from '~/components/Row.vue'
import Column from '~/components/Column.vue'
import Hero from '~/components/Hero.vue'
import Carousel from '~/components/Carousel.vue'
import Slide from '~/components/Slide.vue'
import Card from '~/components/Card.vue'
import Tile from '~/components/Tile.vue'
import logoSpotlight from '~/components/logoSpotlight.vue'
import Generic from '~/components/Generic.vue'
import relatedParts from '~/components/RelatedParts.vue'

Vue.component('page', Page)
Vue.component('row', Row)
Vue.component('hero', Hero)
Vue.component('column', Column)
Vue.component('carousel', Carousel)
Vue.component('slide', Slide)
Vue.component('card', Card)
Vue.component('tile', Tile)
Vue.component('logoSpotlight', logoSpotlight)
Vue.component('generic', Generic)
Vue.component('relatedParts', relatedParts)
